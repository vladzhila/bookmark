class UsersController < ApplicationController
  def index
    @user = User.find_by_id(params[:id])
    redirect_to root_url if @user && @user.id == current_user.id
  end

  def friends
    @friends = User.friends(current_user)
  end

  private

  def user_bookmarks
    @user_bookmarks ||= @user.bookmarks.paginate(page: params[:page], per_page: 5)
  end

  helper_method :user_bookmarks
end
