class SessionsController < ApplicationController
  def new
    redirect_to '/' if current_user
  end

  def create
    auth = request.env["omniauth.auth"]
    session[:omniauth] = auth.except('extra')
    user = User.sign_in_from_omniauth(auth)
    session[:user_id] = user.id
    redirect_to root_url
  end

  def destroy
    session[:user_id] = nil
    session[:omniauth] = nil
    redirect_to root_url
  end
end
