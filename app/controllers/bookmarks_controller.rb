class BookmarksController < ApplicationController
  before_filter :authorize

  def new
    @bookmark = Bookmark.new
  end

  def create
    screenshot_name = Bookmark.make_screenshot(bookmark_params[:url])

    @bookmark = Bookmark.new(
      url: bookmark_params[:url],
      title: bookmark_params[:title],
      image: screenshot_name,
      user_id: current_user.id
    )

    if @bookmark.save
      redirect_to root_url
    else
      redirect_to new_bookmark_url, flash: { error: @bookmark.errors.full_messages }
    end
  end

  def destroy
    bookmark = Bookmark.find_by_id(params[:id])
    bookmark.delete

    if bookmark.destroyed?
      Bookmark.delete_image(bookmark.image)
    end
  end

  private

  def bookmark_params
    params.fetch(:bookmark).permit(:url, :title)
  end

  def bookmarks
    value_search = params[:value_search]
    @bookmarks ||= current_user.bookmarks

    unless value_search.blank?
      @bookmarks = @bookmarks.where(user_id: current_user.id).search(value_search)
    end

    @bookmarks.paginate(page: params[:page], per_page: 5)
  end

  helper_method :bookmarks
end
