class User < ApplicationRecord
  has_many :bookmarks

  def self.sign_in_from_omniauth(auth)
    find_by(provider: auth.provider, uid: auth.uid) || create_user_from_omniauth(auth)
  end

  def self.create_user_from_omniauth(auth)
    create(
      provider: auth.provider,
      uid: auth.uid,
      name: auth.info.name,
      image: auth.info.image,
      auth_token: auth.credentials.token,
      auth_expires_at: Time.at(auth.credentials.expires_at)
    )
  end

  def self.friends(current_user)
    graph = Koala::Facebook::API.new(current_user.auth_token)
    user_friends = graph.get_connections(current_user.uid, 'friends')

    uids = []
    user_friends.each { |f| uids << f['id'] }

    where('uid IN (?)', uids)
  end
end
