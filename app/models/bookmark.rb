class Bookmark < ApplicationRecord
  belongs_to :user

  validates :url, :title, presence: true, allow_blank: false
  validates :url, format: { with: URI.regexp },
                  if: Proc.new { |a| a.url.present? }

  def self.search(value)
    if value
      self.where('url LIKE :search OR title LIKE :search', search: "%#{value}%")
    else
      self.all
    end
  end

  def self.make_screenshot(url)
    screenshot = Gastly.screenshot(url)
    screenshot.browser_width = 1280
    screenshot.browser_height = 720

    image = screenshot.capture
    image.resize(width: 1280, height: 720)

    screen_name = [*('a'..'z'),*('0'..'9')].shuffle[0,8].join
    path_to_image = "#{Rails.public_path}/screenshot/#{screen_name}.png"

    image.save(path_to_image)
    screen_name
  end

  def self.delete_image(img_name)
    path_to_image = "#{Rails.public_path}/screenshot/#{img_name}.png"
    File.delete(path_to_image) if File.exist?(path_to_image)
  end
end
