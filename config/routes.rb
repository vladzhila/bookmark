Rails.application.routes.draw do
  root 'bookmarks#index'

  get 'auth/:provider/callback', to: 'sessions#create'
  get '/login' => 'sessions#new'

  get '/users/:id' => 'users#index'
  get '/users' => 'users#friends'

  delete 'logout', to: 'sessions#destroy', as: 'logout'
  resources :bookmarks
end
